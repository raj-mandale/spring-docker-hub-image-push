1. ec2 setup

install below on ec2
  
 **java**
  
sudo apt-get update
sudo apt install openjdk-17-jre-headless -y
java --version

sudo apt-get remove openjdk-17-jre-headless -y

 **maven**
 
sudo apt update
sudo apt-get install maven -y
mvn --version

sudo apt-get remove maven -y
 
 **docker**

sudo apt-get update
sudo apt install docker.io -y
sudo docker --version

sudo systemctl start docker
sudo systemctl enable docker
sudo systemctl status docker

sudo apt-get remove docker docker-engine docker.io

give the permission to docker.sock to perform the operation on docker

command - chmod 777 /var/run/docker.sock

after stop and start server again give the permission
 
----------------------------------------------------------------------------------------------------------

2.connect runner

goto :- Peoject > settings > CICD > runner >

  install runner before that check the Architecture
  command - dpkg --print-architecture 
  and select ruuner according to architecture
  go to ruuner copy command and paste into ec2 for connection
  give the proper information like
                                                   
Enter the GitLab instance URL (for example, https://gitlab.com/):[https://gitlab.com/]:- hit enter
Enter the registration token:[GR1348941bkm4m-mAhWAUZRfjzuJu]:- hit enter
Enter a description for the runner:[ip-172-31-35-203]:- hit enter
Enter tags for the runner (comma-separated):- give any tag name remember we have to give that name inside yml file (ec2,server)
Enter optional maintenance note for the runner:- hit enter
Enter an executor: docker+machine, kubernetes, instance, custom, shell, parallels, virtualbox, ssh, docker, docker-windows, docker-autoscaler:- shell

goto runner >  click on edit option > and check run without tag --if you want to run withut tag

----------------------------------------------------------------------------------------------------------

3.Shell profile loading - move to root before doing that operation - sudo su

To troubleshoot this error, check 
command - vi /home/gitlab-runner/.bash_logout
For example, if the .bash_logout file has a script section like the following, 
comment it out and restart the pipeline:

before :-

if [ "$SHLVL" = 1 ]; then
    [ -x /usr/bin/clear_console ] && /usr/bin/clear_console -q
fi

after :-

#if [ "$SHLVL" = 1 ]; then
#  		[ -x /usr/bin/clear_console ] && /usr/bin/clear_console -q
#fi

----------------------------------------------------------------------------------------------------------

step 5

7. write the gitlab cicd pipeline
use this default name - (.gitlab-ci.yml)

(Notes - $USERNAME - username of dockerhub 
		$TOKEN - password of dockerhub)
(dont use direct value of username or password instead of that use variablke which is declare in gtilab variable)

.gitlab-ci.yml file
{
variables:
    REGISTRY: $USERNAME/spring-docker-hub-image-push
    TAG: "$CI_COMMIT_SHORT_SHA-$CI_PIPELINE_ID"
stages:
    - build_project
    - build_image
    - push_image_to_contain_reg

build_project:
    stage: build_project
    image:  maven:3.8.3-openjdk-17
    script:
        - echo "Building app..."
        - mvn clean install
        - echo "Finished building the app."
    artifacts:
        expire_in: 1 week
        paths:
            - target/helloworld.jar
    tags:
        - ec2
        - server

build_image:
    stage: build_image
    script:
        - echo "building image"
        - docker build . -t $REGISTRY:$TAG
        - echo "Finished building image."
    tags:
        - ec2
        - server

push_image_to_contain_reg:
    stage: push_image_to_contain_reg
    script:
        - echo "building con"
        - docker login -u $USERNAME -p $TOKEN
        - docker push $REGISTRY:$TAG
        - echo "Finished building con."
    tags:
        - ec2
        - server

}
----------------------------------------------------------------------------------------------------------

